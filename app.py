from flask import Flask, request, render_template
import numpy as np
import tensorflow as tf
from tensorflow import keras
from keras.models import Sequential
from keras.layers import Dense


x = np.zeros(7)

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('matrix_filler2.html', name='Сделаем расчет! Введите r')

def processing_params(param1, param2, param3, param4, param5, param6, param7, param8, param9, param10, param11, param12):
    model = keras.models.load_model(r"/Users/cybercok/Documents/Jupiter/drh/VKR/Models/model23.h5")
    pred = model.predict(
    [param1, param2, param3, param4, param5, param6, param7, param8, param9, param10, param11, param12])
    pred = model.predict(x).flatten()
    message = f"Соотношение матрица-наполнитель = {pred}"
    return message

@app.route('/r/', methods=['post', 'get'])
def login():
    message = ''
    if request.method == 'POST':
        param1 = request.form.get('param1')  # запрос к данным формы
        param2 = request.form.get('param2')
        param3 = request.form.get('param3')
        param4 = request.form.get('param4')
        param5 = request.form.get('param5')
        param6 = request.form.get('param6')
        param7 = request.form.get('param7')
        param8 = request.form.get('param8')
        param9 = request.form.get('param9')
        param10 = request.form.get('param10')
        param11 = request.form.get('param11')
        param12 = request.form.get('param12')



        param1 = float(param1)
        param2 = float(param2)
        param3 = float(param3)
        param4 = float(param4)
        param5 = float(param5)
        param6 = float(param6)
        param7 = float(param7)
        param8 = float(param8)
        param9 = float(param9)
        param10 = float(param10)
        param11 = float(param11)
        param12 = float(param12)


        message = processing_params(param1, param2, param3, param4, param5, param6, param7, param8, param9, param10, param11, param12)
    return render_template('matrix_filler2.html', message=message)

if __name__ == '__main__':
    app.run()